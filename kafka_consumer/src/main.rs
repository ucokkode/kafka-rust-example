use std::fmt::Write;
use std::{env, time::Duration};

use kafka::consumer::GroupOffsetStorage;
use kafka::{
    consumer::{Consumer, FetchOffset},
    producer::{Producer, Record, RequiredAcks},
};

#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct Person {
    name: String,
    age: i32
}


fn main() {
    let topic = "lobby";
    let hosts = vec!["localhost:29092".to_owned()];
    match env::args().nth(1).unwrap().as_str() {
        "consume" => consumer(topic, hosts),
        "producer" => producer(topic, hosts),
        _ => println!("choose the args"),
    }
}

fn producer(topic: &'_ str, hosts: Vec<String>) {
    let mut producer = Producer::from_hosts(hosts)
        .with_ack_timeout(Duration::from_secs(1))
        .with_required_acks(RequiredAcks::One)
        .create()
        .unwrap();

    for i in 0..10 {
        let person = Person {
            name: "person".to_owned(), 
            age: 20 + i
        };
        let bincode = bincode::serialize(&person).unwrap();
        producer
            .send(&Record::from_value(topic, bincode))
            .unwrap();
    }
}

fn consumer(topic: &'_ str, hosts: Vec<String>) {
    let mut consumer = Consumer::from_hosts(hosts)
        .with_fallback_offset(FetchOffset::Earliest)
        .with_topic(topic.to_owned())
        .with_group("my-group".to_owned())
        .with_offset_storage(Some(GroupOffsetStorage::Kafka))
        .create()
        .unwrap();
    loop {
        for msg in consumer.poll().unwrap().iter() {
            for m in msg.messages() {
                let x: Person = bincode::deserialize(m.value).unwrap();
                println!("{:?}", x);
            }
            consumer.consume_messageset(msg).unwrap();
        }
        consumer.commit_consumed().unwrap();
    }
}
